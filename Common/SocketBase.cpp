#include "sockets.h"

// Constructor
CBaseSocket::CBaseSocket( )
{
	sock = INVALID_SOCKET;
	isActive = false;
}

// Destructor
CBaseSocket::~CBaseSocket( )
{
}

// Close all open sockets
void CBaseSocket::CloseSocket( void )
{
	// Leave function, if socket isn't active
	if ( !isActive ) return;
	// Close socket
	close( sock );
	sock	= INVALID_SOCKET;
	isActive	= false;
}

// Init winsocket, if we are using Windows
bool InitWinSocket ( void )
{
#ifdef _WIN32
	WSADATA wsa;
	return (WSAStartup(MAKEWORD(2,0),&wsa)==0);
#else
	return true;
#endif
}

// Close winsocket, if we are using Windows
void CloseWinSocket( void )
{
#ifdef _WIN32
	WSACleanup( );
#endif
}



