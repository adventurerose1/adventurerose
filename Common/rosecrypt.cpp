#include "rosecrypt.hpp"

#ifdef _WIN32
// we will not include this in linux
// build crypt table
void buildCryptTable(char* crypttable, char* checksum, int checksumlen)
{
	for (int i=0; i<0x008; i++) crypttable[i]=0;
	for (int i=0; i<0x100; i++) crypttable[i+8]=i;

	int csumpos=0;
	int modifier=0;

	for (int i=0; i<0x40*4; i++) {
			int curpos=crypttable[8+i];

			modifier = (checksum[csumpos] + curpos + modifier) & 0xff;
			crypttable[8+i] = crypttable[modifier+8];
			crypttable[modifier+8] = curpos;

			csumpos=++csumpos%checksumlen;
	}
}

// en/decrypt packet
void cryptPacket(char *packet, char* crypttable )
{
	unsigned short paksize=(*((unsigned short*)&packet[0])) - 2;
	for(int i=2; i<paksize; i++)
    {
		packet[i] = 0x61 ^ packet[i];
	}
}


#endif
