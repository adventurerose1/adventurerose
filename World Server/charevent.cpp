#include "character.h"

//virtual [called when a enemy hit this character]
bool CCharacter::OnBeAttacked( CCharacter* Enemy )
{
    return true;
}

// virtual [called when this character die]
bool CCharacter::OnDie( )
{
    return true;
}

// virtual [called when enemy die]
bool CCharacter::OnEnemyDie( CCharacter* Enemy )
{
    Log(MSG_INFO,"Virtual CCharacter::OnEnemyDie");
    ClearBattle( Battle );
    return true;
}

// virtual [called when a enmy is on sigh]
bool CCharacter::OnEnemyOnSight( void* Enemy )
{
    return true;
}

// virtual [called when this character spawn]
bool CCharacter::OnSpawn( bool )
{
    return true;
}

// virtual [called when this character is almost dead]
bool CCharacter::OnAlmostDie( )
{
    return true;
}

// virtual [called when character is far]
bool CCharacter::OnFar( )
{
    Log(MSG_INFO,"Virtual CCharacter::OnFar");
    return true;
}
