#include "worldserver.h"

bool CWorldServer::GiveExp( CMonster* thismon, UINT special_lvl, UINT special_exp )
{

    if( thismon->owner != 0) // Summon
	{
        MapList.Index[thismon->Position->Map]->DeleteMonster( thismon );
	    return true;
    }

    bool lma_debug=false;
   if(thismon->Position->respawn==4589)
   {
       lma_debug=true;
       Log(MSG_INFO,"GiveExp Spawn %u CID %u",thismon->Position->respawn,thismon->clientid);
   }

    byte slot = 0;
    dword hash = 0;
   vector<CPartyExp*> PartyExp;
    for(UINT i=0;i<thismon->PlayersDamage.size();i++) //loop through a array with all damage done to a monster by the individual players
    {
        MonsterDamage* thisplayer = thismon->PlayersDamage.at(i); //get this players damage
		CPlayer* thisclient = GetClientByCID( thisplayer->charid, thismon->Position->Map ); //get this players character

		if( thisplayer->damage>0 && thisclient!=NULL && !thisclient->IsDead())
        {
    		if( thisclient->Battle->target == thismon->clientid )
            {
                ClearBattle( thisclient->Battle )
                //thisclient->Position->destiny = thisclient->Position->current; //GIVE EXP FUNCTION
            }
            if( thismon->MonsterDrop->firsthit == thisclient->CharInfo->charid )
            {
                for( int q=0;q<10;q++)
                {
                    // Give Quest Item
                    if( thisclient->quest.quests[q].QuestID!=0 )
                    {

                        int success = thisclient->ExecuteQuestTrigger(thismon->die_quest,true);
                        thisclient->Send_pakQuestReply(success, slot, 0, hash);
                        thisclient->SendQuestUpdate();

                        break;
                    }
                }
            }

            unsigned int playerDamageContribution;
            if(thisplayer->damage > thismon->thisnpc->hp || thisclient->Party->party != NULL) {
                playerDamageContribution = 1;
            } else {
                playerDamageContribution = thisplayer->damage / thismon->thisnpc->hp;
            }
            unsigned int exp = (unsigned int)ceil((thismon->thisnpc->exp * playerDamageContribution) + special_exp);

            exp = exp * (unsigned long long) (Config.EXP_RATE);

            if( thisclient->Party->party!=NULL ) //player is part of a party
            {
                bool pflag = false;

                for(int i=0;i<PartyExp.size();i++)
                {
                    CPartyExp* thisparty = PartyExp.at( i );
                    if( thisparty->thisparty == thisclient->Party->party )
                    {
                        //LMA: We get exp only if we deserve it.
                        //thisparty->exp += exp;
                        thisparty->exp += GetColorExp( thisclient->Stats->Level, thismon->thisnpc->level + special_lvl, exp );
                        //LMA: bug...
                        //thisparty->exp += exp * (unsigned long long) ((thisclient->Party->party->PartyLevel*2) / 100);
                        thisparty->exp += (unsigned long long) ((exp*thisclient->Party->party->PartyLevel*3) / 200);
                        pflag = true;
                    }
                }

                if( !pflag )
                {
                    CPartyExp* thisparty = new CPartyExp;
                    thisparty->thisparty = thisclient->Party->party;
                    //LMA: We get exp only if we deserve it.
                    thisparty->exp = exp;
                    thisparty->exp = GetColorExp( thisclient->Stats->Level, thismon->thisnpc->level + special_lvl, exp );
                    thisparty->flag = false;
                    //LMA: bug
                    //thisparty->exp += exp * (unsigned long long) ((thisclient->Party->party->PartyLevel*2) / 100);
                    thisparty->exp += (unsigned long long) ((exp*thisclient->Party->party->PartyLevel*3) / 600);
                    thisparty->num = 1;
                    thisparty->partymember[0] = thisclient->CharInfo->charid;
                    thisparty->cheat_max_lvl=thisclient->Stats->Level;
                    thisparty->cheat_min_lvl=thisclient->Stats->Level;
                    thisparty->maxlevel = thisclient->Stats->Level;
                    for(unsigned int p=0;p<ClientList.size();p++)
                    {
                        CPlayer* otherclient = (CPlayer*) ClientList.at( p )->player;
                        if(otherclient->client==NULL) continue;
                        if(otherclient->Party->party==NULL) continue;
                        if(!otherclient->client->isActive) continue;
                        if(!otherclient->Session->inGame) continue;

                        //LMA: checking the gap between the max and min level of people in the party.
                        if (thisclient->Party->party == otherclient->Party->party)
                        {
                            if(otherclient->Stats->Level>thisparty->cheat_max_lvl)
                            {
                                thisparty->cheat_max_lvl=otherclient->Stats->Level;
                            }

                            if(otherclient->Stats->Level<thisparty->cheat_min_lvl)
                            {
                                thisparty->cheat_min_lvl=otherclient->Stats->Level;
                            }

                        }

                        if( IsVisible(thisclient, otherclient))
                        {
                            if(thisclient->Party->party == otherclient->Party->party )
                            {
                                //LMA: Little stupid check.
                                if (thisparty->num==7)
                                {
                                    Log(MSG_WARNING,"Too many people in this party, already at max, skipping");
                                    break;
                                }

                                thisparty->partymember[thisparty->num] = otherclient->CharInfo->charid;
                                thisparty->num++;
                                thisparty->maxlevel += otherclient->Stats->Level;
                            }
                        }
                    }
                    PartyExp.push_back( thisparty );
                }
                continue;
            }


            if(!thisclient->no_exp && thisclient->Stats->Level < GServer->Config.MaxLevel)
            {
                thisclient->CharInfo->Exp +=  thisclient->bonusxp*GetColorExp( thisclient->Stats->Level, thismon->thisnpc->level + special_lvl, exp );
            }

            if(thisclient->CharInfo->Exp<thisclient->GetLevelEXP())
            {
                //LMA: TEST
                //Log(MSG_INFO,"new exp in giveexp %I64i",thisclient->CharInfo->Exp);
                BEGINPACKET( pak, 0x79b );
                ADDDWORD   ( pak, thisclient->CharInfo->Exp );
                ADDWORD    ( pak, thisclient->CharInfo->stamina );
                //ADDWORD    ( pak, 0 );
                ADDWORD    ( pak, thismon->clientid );

                if (thisclient->Position->Map== 22){
                thisclient->CharInfo->Valor++;
                SendSysMsg(thisclient,"You have earned a Valor Point RELOG to update your points.");}

                thisclient->client->SendPacket( &pak );
            }

        }

    }


    for(int p=0;p<PartyExp.size();p++)
    {
        CPartyExp* thisparty = PartyExp.at( p );
        //LMA: End of HIGHRATEPARTYEXPNONE.

        for(int i=0;i<thisparty->num;i++)
        {
            CPlayer* partyclient = GetClientByCID( thisparty->partymember[i], thismon->Position->Map );
            if( partyclient==NULL )
                continue;
            if(partyclient->Party->party==NULL)
                continue;

            //LMA: no exp if a player has a too low level...
            //Uncomment this part of code below if you want to use it... (HIGHRATEPARTYEXP)

            //Uncomment should end here for HIGHRATEPARTYEXP.

            if(!thisparty->flag)
            {
                //partyclient->Party->party->Exp += (thisparty->exp / 6) / 2;
                partyclient->Party->party->Exp += (thisparty->exp / 60) / 60; //Tomiz: thisparty->exp decreased
                if( partyclient->Party->party->PartyLevel == 50)
                {
                    partyclient->Party->party->Exp = 0;
                }

                //New code.
                bool new_level=false;
                while(partyclient->Party->party->PartyLevel<50 && partyclient->Party->party->Exp >= GetMaxPartyExp( partyclient->Party->party->PartyLevel ) )
                {
                    partyclient->Party->party->PartyLevel++;
                    partyclient->Party->party->Exp -= GetMaxPartyExp( partyclient->Party->party->PartyLevel-1 );
                    Log(MSG_INFO,"Going from party level %i to %i, exp left %I64i",partyclient->Party->party->PartyLevel-1,partyclient->Party->party->PartyLevel,partyclient->Party->party->Exp);
                    new_level=true;
                }

                if( partyclient->Party->party->PartyLevel == 50)
                {
                    partyclient->Party->party->Exp = 0;
                }

                //Strange thing when a new level:
                unsigned long long send_exp=partyclient->Party->party->Exp;
                if(new_level)
                {
                    send_exp+=0x80000000;
                }

                Log(MSG_INFO,"Exp sent: %I64i",send_exp);
                //End of code.

                BEGINPACKET( pak, 0x7d4 );
                ADDBYTE    ( pak, partyclient->Party->party->PartyLevel );

                //LMA: little change.
                //ADDDWORD   ( pak, partyclient->Party->party->Exp );
                ADDDWORD   ( pak, send_exp );

                partyclient->Party->party->SendToMembers( &pak );
                thisparty->flag = true;
            }

            //LMA: no exp for dead people.
            if(partyclient->IsDead())
            {
                Log(MSG_INFO,"No exp for player %s, he's dead.",partyclient->CharInfo->charname);
                continue;
            }

            //LMA Begin
            //20070621-211100
            //mods for CF
            unsigned int expoption = partyclient->Party->party->Option%0x80;
            //LMA: Exp nullfier.
            if(!partyclient->no_exp)
            {
                if( expoption==0 )
                {
					if(partyclient->Stats->Level < Config.MaxLevel)
                    partyclient->CharInfo->Exp +=  GetColorExp( partyclient->Stats->Level, thismon->Stats->Level + special_lvl, (UINT)round(thisparty->exp / thisparty->num) );
                }
                else
                {
					if(partyclient->Stats->Level < Config.MaxLevel)
                    partyclient->CharInfo->Exp +=  GetColorExp( partyclient->Stats->Level, thismon->Stats->Level + special_lvl, (UINT)round(partyclient->Stats->Level * thisparty->exp / thisparty->maxlevel) );
                }

            }
            //LMA END

            if(partyclient->CharInfo->Exp<partyclient->GetLevelEXP())
            {
                //LMA: TEST
                //Log(MSG_INFO,"new exp in giveexp %I64i",partyclient->CharInfo->Exp);
                BEGINPACKET( pak, 0x79b );
                ADDDWORD   ( pak, partyclient->CharInfo->Exp );
                ADDWORD    ( pak, partyclient->CharInfo->stamina );
                //ADDWORD    ( pak, 0 );
                ADDWORD    ( pak, thismon->clientid );
                partyclient->client->SendPacket( &pak );
            }

        }
    }

    //LMA: cleaning time.
    for(int p=0;p<PartyExp.size();p++)
    {
        //It seems it's ok, the class party destructor isn't beeing called...
        delete PartyExp.at(p);
    }

    if (lma_debug)
    {
        Log(MSG_INFO,"END GiveExp Spawn %u CID %u",thismon->Position->respawn,thismon->clientid);
    }
    CPlayer* thisclient = GServer->GetClientByCID(thismon->MonsterDrop->firsthit);
    if(thismon->team != 0 && thismon->montype == 3708 && thismon->Position->Map == 148)
    {
        Log(MSG_INFO,"CAlled because %i montype",thismon->montype);
        MapList.Index[thismon->Position->Map]->UpdateArena( thisclient );
    }
    MapList.Index[thismon->Position->Map]->DeleteMonster( thismon );


    return true;
}
