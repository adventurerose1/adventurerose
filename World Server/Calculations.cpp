#include "worldserver.h"

int CWorldServer::GetSuccessRATE( CPlayer* pATK, CPlayer* pDEF )
{
    int iRAND1, iRAND2;
	int iSuccess;

	if ( pATK->IsPlayer() )
	{
		if ( pDEF->IsPlayer() )
		{
			iRAND1 = GServer->RandNumber(1,100);
            iSuccess = (float)( 90 - ((pATK->Stats->Accuracy + pDEF->Stats->Dodge )/ pATK->Stats->Accuracy ) * 40 + iRAND1 );
		}
		else
		{
			iRAND1 = GServer->RandNumber(1,50);
			iRAND2 = GServer->RandNumber(1,60);
			iSuccess = (int)( (pATK->Stats->Level + 10) - pDEF->Stats->Level * 1.1 + (iRAND1) );
			if ( iSuccess <= 0 )
				return 0;

			return (int)( iSuccess * (pATK->Stats->Accuracy*1.1 - pDEF->Stats->Dodge*0.93 + iRAND2 + 5 + pATK->Stats->Level * 0.2 ) / 80 );
		}
	}
	else
	{
        iRAND1 = GServer->RandNumber(1,100);
        iSuccess = 138 - ( (float)( pATK->Stats->Accuracy + pDEF->Stats->Dodge ) / pATK->Stats->Accuracy ) * 75.0 + iRAND1;
        if ( iSuccess <= 0 )
				return 0;
	}
	return iSuccess;
}

int CWorldServer::Get_CriSuccessRATE( CPlayer* pATK )
{
	int iCriSuc = 0;
    if( pATK->IsPlayer() )
        iCriSuc = (int)(28-(( pATK->Stats->Critical / 2 + pATK->Stats->Level ) / ( pATK->Stats->Level + 8 ) ) * 20 ) + GServer->RandNumber(1,100);
    else
    {
        iCriSuc = GServer->RandNumber(1,100);
    }


	return iCriSuc;
}

int CWorldServer::Get_BasicDAMAGE (CPlayer *pATK, CPlayer *pDEF, int iSuc)
{
	int iDamage;

		// Normal damage
		if ( pATK->IsPlayer() && pDEF->IsPlayer() )
		{
			iDamage = (float)( pATK->Stats->Attack_Power * ( (float)pATK->Stats->Level/pDEF->Stats->Level )
				* 26  * ( 1.8 * pATK->Stats->Attack_Power - pDEF->Stats->Defense +150 )
				/ (1.1 * pDEF->Stats->Defense + pDEF->Stats->Dodge * 0.4 + 50 ) / 126);
		}
		else
		{
		    if(pATK->IsPlayer()) iDamage = (float)( (pATK->Stats->Attack_Power * 35 *(1.8* pATK->Stats->Attack_Power - pDEF->Stats->Defense +500)) / ((1.8* pDEF->Stats->Defense + pDEF->Stats->Dodge * 0.4+50)/140));
            else iDamage = (float)( pATK->Stats->Attack_Power * 35 *(2.5* pATK->Stats->Attack_Power - pDEF->Stats->Defense +500) / (1.1* pDEF->Stats->Defense + pDEF->Stats->Dodge * 0.4+50)/50 );

		}
		if ( pATK->Stats->ExtraDamage_add != 0 ) {
			iDamage += (float)( iDamage * pATK->Stats->ExtraDamage_add / 100 );
			Log(MSG_HACK,"Calculation2 iDamage = %i",iDamage);
		}

		iDamage *= 2;
        Log(MSG_HACK,"Calculation3 iDamage = %i",iDamage);

		if ( iDamage < 5 ) iDamage = 5;
		else
		{
			if ( pATK->IsPlayer() && pDEF->IsPlayer() )
			{
				int iMaxDmg = (float)( pDEF->Stats->MaxHP*0.25f );
				if ( iDamage > iMaxDmg ) iDamage = iMaxDmg;
			} else
				if ( iDamage > 9999 ) iDamage = 9999;
		}

	return iDamage;
}
